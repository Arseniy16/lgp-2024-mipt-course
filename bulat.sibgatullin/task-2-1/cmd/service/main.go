package main

import (
	"fmt"
)

type validTemps struct {
	min int
	max int
}

func main() {
	var N, K, temp int
	var compareType string

	_, err := fmt.Scanf("%d\n", &N)
	if err != nil {
		fmt.Println(err)
		return
	}

	officeTemps := make([]validTemps, N)

	// for every department
	for i := 0; i < N; i++ {
		_, err = fmt.Scanf("%d\n", &K)
		if err != nil {
			fmt.Println(err)
			return
		}

		officeTemps[i].max = 30
		officeTemps[i].min = 15

		// for every worker
		for j := 0; j < K; j++ {
			_, err = fmt.Scanf("%s %d\n", &compareType, &temp)
			if err != nil {
				fmt.Println(err)
				return
			}

			if compareType == ">=" {
				if officeTemps[i].min < temp {
					officeTemps[i].min = temp
				}
			} else if compareType == "<=" {
				if officeTemps[i].max > temp {
					officeTemps[i].max = temp
				}
			} else {
				fmt.Println("Incorrect operand, please type >= or <=!")
			}

			if officeTemps[i].min <= officeTemps[i].max && officeTemps[i].min >= 15 && officeTemps[i].max <= 30 {
				temp = officeTemps[i].min
			} else {
				temp = -1
			}

			fmt.Println(temp)
		}
		fmt.Println()
	}
}
