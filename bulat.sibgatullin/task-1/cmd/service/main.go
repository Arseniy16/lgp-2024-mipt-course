package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	reader := bufio.NewReader(os.Stdin)
	var num1, num2, res float64
	var str_op string
	_, err := strconv.ParseFloat("1", 64)

	for { //Using cycle to make program run until correct values are passed
		fmt.Println("Input first num: ")
		str_num1, _ := reader.ReadString('\n')
		str_num1 = str_num1[:len(str_num1)-2]

		num1, err = strconv.ParseFloat(str_num1, 64)
		if err != nil {
			fmt.Printf("%s not a number. Please write correct value.\n", str_num1)
			continue
		}

		break
	}

	for {
		fmt.Println("Input second num: ")
		str_num2, _ := reader.ReadString('\n')
		str_num2 = str_num2[:len(str_num2)-2]

		num2, err = strconv.ParseFloat(str_num2, 64)
		if err != nil {
			fmt.Printf("%s not a number. Please write correct value.\n", str_num2)
			continue
		}

		break
	}

	for {
		fmt.Println("Input operation: ")
		str_op, _ = reader.ReadString('\n')
		str_op = str_op[:len(str_op)-2]

		switch str_op {
		case "+":
			res = num1 + num2
		case "-":
			res = num1 - num2
		case "/":
			if num2 == 0 {
				fmt.Printf("%s is zero - impossible operation.\n", str_op)
				continue
			}
			res = num1 / num2
		case "*":
			res = num1 * num2
		default:
			fmt.Printf("%s is unrecognized operation, please use +, -, / or *.\n", str_op)
			continue
		}

		break
	}

	// Got correct values, show result and exit
	fmt.Printf("Got result of operation: %v %s %v = %v", num1, str_op, num2, res)
}
