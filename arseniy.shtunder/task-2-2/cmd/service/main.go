package main

import (
	"container/heap"
	"fmt"
)

type IntHeap []int

func (h IntHeap) Len() int           { return len(h) }
func (h IntHeap) Less(i, j int) bool { return h[i] > h[j] }
func (h IntHeap) Swap(i, j int)      { h[i], h[j] = h[j], h[i] }

func (h *IntHeap) Push(x any) {
	*h = append(*h, x.(int))
}

func (h *IntHeap) Pop() any {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}

func main() {
	var numDishes, priority, dish int

	_, err := fmt.Scanln(&numDishes)
	if err != nil {
		fmt.Println(err)
		return
	}

	h := &IntHeap{}
	heap.Init(h)

	for i := 0; i < numDishes; i++ {
		_, err = fmt.Scan(&priority)
		if err != nil {
			fmt.Println(err)
			return
		}
		heap.Push(h, priority)
	}

	_, err = fmt.Scanln(&dish)
	if err != nil {
		fmt.Println(err)
		return
	}
	if dish > h.Len() || dish <= 0 {
		fmt.Println("error: preffered dish must be in range [1 : numDishes]")
		return
	}

	for i := 1; i < dish; i++ {
		heap.Pop(h)
	}

	fmt.Printf("%d\n", (*h)[0]) // print preffered dish
}
