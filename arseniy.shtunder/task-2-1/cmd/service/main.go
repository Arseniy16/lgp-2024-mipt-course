package main

import (
	"fmt"
)

func main() {
	var N, K, temp, tMin, tMax int // N - count of departments, K - count of employees
	var expr string

	_, err := fmt.Scanf("%d\n", &N)
	if err != nil {
		fmt.Println(err)
		return
	}

	for i := 0; i < N; i++ {
		_, err := fmt.Scanf("%d\n", &K)
		if err != nil {
			fmt.Println(err)
			return
		}

		// limit temperatures
		tMin = 15
		tMax = 30

		for j := 0; j < K; j++ {
			_, err := fmt.Scanf("%s %d\n", &expr, &temp)
			if err != nil {
				fmt.Println(err)
				return
			}

			switch expr {
			case ">=":
				if temp >= tMin {
					tMin = temp
				}
			case "<=":
				if temp <= tMax {
					tMax = temp
				}
			default:
				fmt.Println("Invalid expression")
				return
			}

			if (tMax >= tMin) && (tMin >= 15) && (tMax <= 30) {
				fmt.Println(tMin)
			} else {
				if tMin > 30 {
					tMin = 15
				}
				if tMax < 15 {
					tMax = 30
				}
				fmt.Println(-1)
			}
		}
		fmt.Println()
	}
}
