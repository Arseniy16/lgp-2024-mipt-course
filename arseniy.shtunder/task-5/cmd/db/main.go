package main

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq"
	dbPack "gitlab.com/arseniy.shtunder/task-5/internal/db"
)

func main() {
	connStr := "user=username dbname=mydb sslmode=disable"

	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	dbService := dbPack.New(db)

	names, err := dbService.GetNames()
	if err != nil {
		fmt.Printf("Error to get names: %s\n", err.Error())
		return
	}

	for _, name := range names {
		fmt.Println(name)
	}
}
